<?php
namespace Ytoune\MyTest20190704;

class TestDump {

	private $pre = '';
	function prefix(?string $code = null) {
		if (null === $code) return $this->pre;
		$this->pre = $pre;
		return $this;
	}

	private $suf = '';
	function suffix(?string $code = null) {
		if (null === $code) return $this->suf;
		$this->suf = $suf;
		return $this;
	}

	function make(string $code): string {
		return $this->pre . $code . $this->suf;
	}

	function say(string $code) {
		echo $this->make($code);
	}

}